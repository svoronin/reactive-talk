Basic project that demostrates possible applications of [rxJava v.2](https://github.com/ReactiveX/RxJava) framework.

Intended for presenting on [First meet-up](https://facebook.com/events/1505555909456183/) of [Dnipro Reactive Community](https://www.facebook.com/DniproReactive/), which has been held in Dnipro on March 28, 2017. Hosted and organized by SoftServe.

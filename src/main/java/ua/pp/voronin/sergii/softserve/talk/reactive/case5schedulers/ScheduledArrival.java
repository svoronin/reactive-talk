package ua.pp.voronin.sergii.softserve.talk.reactive.case5schedulers;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import ua.pp.voronin.sergii.softserve.talk.reactive.common.ObservableNewBridge;
import ua.pp.voronin.sergii.softserve.talk.reactive.model.Person;

public class ScheduledArrival {

    public static void main(String[] args) throws InterruptedException {
        new ObservableNewBridge()
                .flatMap(citybus -> Observable.fromIterable(citybus.getPassengers()))
                .subscribeOn(Schedulers.computation())
                .doOnNext(person -> logThreadInfo("Sending", person))
                .observeOn(Schedulers.io())
                .subscribe(person -> logThreadInfo("Arrived",person));

        Thread.sleep(5000); // Do not let main thread to finish before bg Threads get to work
    }

    private static void logThreadInfo(String prefix, Person person) {
        System.out.println(prefix + " " + person + " " + Thread.currentThread().getName());
    }
}

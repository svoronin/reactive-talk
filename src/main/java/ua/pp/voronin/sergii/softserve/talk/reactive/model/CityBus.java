package ua.pp.voronin.sergii.softserve.talk.reactive.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class CityBus {
    private List<Person> passengers = new LinkedList<>();

    public CityBus(Person ... passengers) {
        Collections.addAll(this.passengers, passengers);
    }

    public List<Person> getPassengers() {
        return passengers;
    }
}

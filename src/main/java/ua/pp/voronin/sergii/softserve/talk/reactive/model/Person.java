package ua.pp.voronin.sergii.softserve.talk.reactive.model;

public class Person {

    private String name;

    public Person() {}

    public Person withName(String name) {
        this.name = name;
        return this;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() +
                "['name':'" + getName() + "']";
    }
}

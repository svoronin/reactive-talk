package ua.pp.voronin.sergii.softserve.talk.reactive.flow;

import java.util.concurrent.Flow;

public class PublisherEastCoastPassengers implements Flow.Publisher<Passenger> {

    @Override
    public void subscribe(Flow.Subscriber<? super Passenger> subscriber) {
        subscriber.onNext(new Passenger().withName("Vlas"));
        subscriber.onNext(new Passenger().withName("Kseniia"));
        subscriber.onNext(new Passenger().withName("Sergii"));
        subscriber.onComplete();
    }
}

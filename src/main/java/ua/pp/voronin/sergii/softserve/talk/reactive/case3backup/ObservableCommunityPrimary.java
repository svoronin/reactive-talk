package ua.pp.voronin.sergii.softserve.talk.reactive.case3backup;

import io.reactivex.Observable;
import io.reactivex.Observer;
import ua.pp.voronin.sergii.softserve.talk.reactive.model.Person;

import java.util.Arrays;
import java.util.Random;

/*    All the fame goes to Yakov Fain (Farata Systems)    */

public class ObservableCommunityPrimary extends Observable<Person> {
    private Random random = new Random();
    private Iterable<Person> people = Arrays.asList(
            new Person().withName("Vlas"),
            new Person().withName("Kseniia"),
            new Person().withName("Sergii")
    );

    protected void subscribeActual(Observer<? super Person> observer) {
        try {
            for (Person p : people) {
                Thread.sleep(500); // Emulating delay in getting data

                if (random.nextInt(1000) < 250) { // Emulating data error
                    System.out.println("*** Exception in current data source ***");
                    throw new Exception(p + " was late");
                }
                observer.onNext(p);
            }
            observer.onComplete();
        } catch (Exception e) {
            observer.onError(e);
        }
    }
}

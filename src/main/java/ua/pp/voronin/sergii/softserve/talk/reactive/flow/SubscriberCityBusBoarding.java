package ua.pp.voronin.sergii.softserve.talk.reactive.flow;

import java.util.concurrent.Flow;

public class SubscriberCityBusBoarding implements Flow.Subscriber<Passenger> {

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        subscription.request(100);
    }

    @Override
    public void onNext(Passenger passenger) {
        System.out.println(passenger.getName() + " boarded on the bus");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println("Boarding was interrupted due to: " + throwable.getMessage());
    }

    @Override
    public void onComplete() {
        System.out.println("All passengers have been boarded successfully");
    }
}

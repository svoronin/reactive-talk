package ua.pp.voronin.sergii.softserve.talk.reactive.flow;

import java.util.concurrent.Flow;

public class Runner {

    public static void main(String[] args) {
        Flow.Publisher<Passenger> eastCoastPassengers = new PublisherEastCoastPassengers();
        Flow.Subscriber<Passenger> cityBus = new SubscriberCityBusBoarding();
        eastCoastPassengers.subscribe(cityBus);
    }
}

package ua.pp.voronin.sergii.softserve.talk.reactive.case3backup;

import io.reactivex.Observable;
import io.reactivex.Observer;
import ua.pp.voronin.sergii.softserve.talk.reactive.common.ObserverCityBus;
import ua.pp.voronin.sergii.softserve.talk.reactive.model.Person;

public class CityBusBoarding {

    public static void main(String[] args) {
        Observable<Person> passengers = new ObservableCommunityPrimary();
        Observer<Person> cityBus = new ObserverCityBus();
        passengers.subscribe(cityBus);
    }
}

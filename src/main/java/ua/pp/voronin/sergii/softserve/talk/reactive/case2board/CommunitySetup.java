package ua.pp.voronin.sergii.softserve.talk.reactive.case2board;

import io.reactivex.Observable;
import io.reactivex.Observer;
import ua.pp.voronin.sergii.softserve.talk.reactive.common.ObservableCommunity;
import ua.pp.voronin.sergii.softserve.talk.reactive.common.ObserverCityBus;
import ua.pp.voronin.sergii.softserve.talk.reactive.model.Person;

public class CommunitySetup {

    public static void main(String[] args) {
        Observable<Person> eastCoastPassengers = new ObservableCommunity();
        Observer<Person> cityBus = new ObserverCityBus();
        eastCoastPassengers.subscribe(cityBus);
    }
}

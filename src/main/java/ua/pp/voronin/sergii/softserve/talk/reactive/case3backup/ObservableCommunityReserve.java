package ua.pp.voronin.sergii.softserve.talk.reactive.case3backup;

import io.reactivex.Observable;
import io.reactivex.Observer;
import ua.pp.voronin.sergii.softserve.talk.reactive.model.Person;

import java.util.Arrays;

/*    All the fame goes to Yakov Fain (Farata Systems)    */

public class ObservableCommunityReserve extends Observable<Person> {
    private Iterable<Person> people = Arrays.asList(
            new Person().withName("Artur"),
            new Person().withName("Anastasiia"),
            new Person().withName("Denys")
    );

    protected void subscribeActual(Observer<? super Person> observer) {
        for (Person p : people) {
            try {
                Thread.sleep(500); // Emulating delay in getting data
            } catch (InterruptedException e) {
                observer.onError(e);
            }
            observer.onNext(p);
        }
        observer.onComplete();
    }
}

package ua.pp.voronin.sergii.softserve.talk.reactive.common;

import io.reactivex.observers.DefaultObserver;
import ua.pp.voronin.sergii.softserve.talk.reactive.model.Person;

public class ObserverCityBus extends DefaultObserver<Person> {

    public void onNext(Person person) {
        System.out.println(person.getName() + " arrived.");
    }

    public void onError(Throwable throwable) {
        System.out.println("Boarding was interrupted due to: " + throwable.getMessage());
    }

    public void onComplete() {
        System.out.println("All passengers have been boarded successfully");
    }
}

package ua.pp.voronin.sergii.softserve.talk.reactive.case1sout;

import io.reactivex.Observable;
import io.reactivex.observers.DefaultObserver;
import ua.pp.voronin.sergii.softserve.talk.reactive.common.ObservableCommunity;
import ua.pp.voronin.sergii.softserve.talk.reactive.model.Person;

public class Output {

    public static void main(String[] args) {
        Observable<Person> people = new ObservableCommunity();
        people.subscribe(
                new DefaultObserver<Person>() {
                    @Override
                    public void onNext(Person person) {
                        System.out.println(person);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        System.err.println(throwable.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("Here we are!");
                    }
             }
        );/*
        people.subscribe(
                System.out::println,
                System.err::println,
                () -> System.out.println("Here we are!")
        );*/
    }
}

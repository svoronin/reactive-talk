package ua.pp.voronin.sergii.softserve.talk.reactive.case3backup;

import io.reactivex.Observable;
import io.reactivex.Observer;
import ua.pp.voronin.sergii.softserve.talk.reactive.common.ObserverCityBus;
import ua.pp.voronin.sergii.softserve.talk.reactive.model.Person;

public class CityBusBoardingSafe {

    public static void main(String[] args) {
        Observable<Person> passengers = new ObservableCommunityPrimary();
        Observable<Person> reserve = new ObservableCommunityReserve();

        Observable<Person> passangersWithFallback = passengers.onErrorResumeNext(reserve);

        Observer<Person> cityBus = new ObserverCityBus();
        passangersWithFallback.subscribe(cityBus);
    }
}

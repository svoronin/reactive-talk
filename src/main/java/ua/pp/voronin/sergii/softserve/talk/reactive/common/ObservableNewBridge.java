package ua.pp.voronin.sergii.softserve.talk.reactive.common;

import io.reactivex.Observable;
import io.reactivex.Observer;
import ua.pp.voronin.sergii.softserve.talk.reactive.model.CityBus;
import ua.pp.voronin.sergii.softserve.talk.reactive.model.Person;

public class ObservableNewBridge  extends Observable<CityBus> {

    @Override
    protected void subscribeActual(Observer<? super CityBus> observer) {
        observer.onNext(new CityBus(
                new Person().withName("Vlas"),
                new Person().withName("Kseniia"),
                new Person().withName("Sergii"))
        );
        observer.onNext(new CityBus(
                new Person().withName("Artur"),
                new Person().withName("Anastasiia"),
                new Person().withName("Denys")
        ));
    }
}

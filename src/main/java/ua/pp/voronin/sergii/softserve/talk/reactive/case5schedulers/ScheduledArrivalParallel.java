package ua.pp.voronin.sergii.softserve.talk.reactive.case5schedulers;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import ua.pp.voronin.sergii.softserve.talk.reactive.common.ObservableNewBridge;
import ua.pp.voronin.sergii.softserve.talk.reactive.model.Person;

public class ScheduledArrivalParallel {

    public static void main(String[] args) throws InterruptedException {
        new ObservableNewBridge()
                .flatMap(cityBus -> Observable.fromIterable(cityBus.getPassengers()))
                // Creating a lot of Observables using specified strategy and then flat-map them back
                // into a single flow
                .flatMap(person -> Observable.just(person)
                        .subscribeOn(Schedulers.computation())
                        .map(p -> {
                            logThreadInfo("Sending", p);
                            return preparePerson(p);
                        })
                )
                /* To affect accepting part, we can also tune in Scheduling on parent Observable
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                */
                .subscribe(person -> logThreadInfo("Arrived", person));

        Thread.sleep(5000); // Do not let main thread to finish before bg Threads get to work
    }

    private static void logThreadInfo(String prefix, Person person) {
        System.out.println(prefix + " " + person + " " + Thread.currentThread().getName());
    }

    private static Person preparePerson(Person p) {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return p;
    }
}

package ua.pp.voronin.sergii.softserve.talk.reactive.flow;

public class Passenger {

    private String name;

    public Passenger() {}

    public Passenger withName(String name) {
        this.name = name;
        return this;
    }

    public String getName() {
        return name;
    }
}

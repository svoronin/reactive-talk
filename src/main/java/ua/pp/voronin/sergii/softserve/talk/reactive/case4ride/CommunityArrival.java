package ua.pp.voronin.sergii.softserve.talk.reactive.case4ride;

import io.reactivex.Observable;
import ua.pp.voronin.sergii.softserve.talk.reactive.common.ObservableNewBridge;

public class CommunityArrival {

    public static void main(String[] args) {
        new ObservableNewBridge()
                .flatMap(citybus -> Observable.fromIterable(citybus.getPassengers()))
                .subscribe(
                        person -> System.out.println(person.getName() + " arrived at Reactive Community"),
                        throwable -> System.err.println(throwable.getMessage()),
                        () -> System.out.println("Reactive Community is all set")
                );
    }
}

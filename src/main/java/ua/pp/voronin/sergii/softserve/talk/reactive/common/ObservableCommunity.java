package ua.pp.voronin.sergii.softserve.talk.reactive.common;

import io.reactivex.Observable;
import io.reactivex.Observer;
import ua.pp.voronin.sergii.softserve.talk.reactive.model.Person;

import java.util.Arrays;

/*    All the fame goes to Yakov Fain (Farata Systems)    */

public class ObservableCommunity extends Observable<Person> {
    private Iterable<Person> people = Arrays.asList(
            new Person().withName("Vlas"),
            new Person().withName("Kseniia"),
            new Person().withName("Sergii")
    );

    protected void subscribeActual(Observer<? super Person> observer) {
        for (Person p : people) {
            try {
                Thread.sleep(500); // Emulating delay in getting data
            } catch (InterruptedException e) {
                observer.onError(e);
            }
            observer.onNext(p);
        }
        observer.onComplete();
    }
}
